package com.smertos.bin.init

import com.smertos.bin.tileentity.BasePoweredTileEntity
import net.minecraftforge.fml.common.registry.GameRegistry.registerTileEntity
import sun.security.x509.ReasonFlags.UNUSED
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent as SE

@Suppress(UNUSED)
object BINTileEntities {

    val tiles = mapOf(
            Pair("te.powered.base", BasePoweredTileEntity::class.java)
    )

    @Suppress(UNUSED)
    fun register() {
        tiles.forEach { registerTileEntity(it.value, it.key) }
    }
}