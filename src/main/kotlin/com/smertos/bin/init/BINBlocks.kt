package com.smertos.bin.init

import com.smertos.bin.blocks.BasePoweredBlock
import com.smertos.bin.blocks.NetworkCoreBlock
import net.minecraft.block.Block
import net.minecraft.item.ItemBlock
import net.minecraftforge.fml.common.registry.GameRegistry
import sun.security.x509.ReasonFlags.UNUSED

@Suppress(UNUSED)
object BINBlocks {

    val baseBlock = BasePoweredBlock("base", 16000)
    val networkCoreBlock = NetworkCoreBlock()

    val blocks: List<BasePoweredBlock> = listOf(
            baseBlock,
            networkCoreBlock
    )

    val itemBlocks: List<ItemBlock> = blocks.map(fun(b: BasePoweredBlock): ItemBlock {
        val itemBlock = (ItemBlock.getItemFromBlock(b) ?: ItemBlock(b)).setRegistryName(b.bName) as ItemBlock
        b.registerItemModel(itemBlock)
        return itemBlock
    })

    fun register() {
        blocks.forEach { e -> GameRegistry.register(e) }
        itemBlocks.forEach { e -> GameRegistry.register(e) }
    }

}