package com.smertos.bin.proxy

import com.smertos.bin.init.BINBlocks
import com.smertos.bin.init.BINTileEntities
import net.minecraft.item.Item
import com.smertos.bin.log

open class CommonProxy {

    open fun preInit() {
        BINBlocks.register()
        BINTileEntities.register()
        log.info("preInit")
    }

    open fun init() {
        log.info("init")
    }

    open fun postInit() {
        log.info("postInit")

    }

    open fun registerItemRenderer(item: Item, meta: Int, id: String) {}
}