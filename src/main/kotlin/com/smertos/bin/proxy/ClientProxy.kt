package com.smertos.bin.proxy

import com.smertos.bin.BetterItemNetworksMain.modid
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.CLIENT)
class ClientProxy: CommonProxy() {

    override fun preInit() {
        super.preInit()
    }

    override fun init() {
        super.init()
    }

    override fun postInit() {
        super.postInit()
    }

    override fun registerItemRenderer(item: Item, meta: Int, id: String) = ModelLoader.setCustomModelResourceLocation(item, meta, ModelResourceLocation("$modid:$id", "inventory"))
}