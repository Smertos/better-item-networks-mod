package com.smertos.bin.network

// This interface used to mark whenever block is capable of connecting to out network.
// I think it's better so split this class into additional api project
// But for now, it will stay here
// It is an experiment at all :)
interface INetworkable {

    // Marks whenever given block is connected to any network
    // This field is not ment to be edited by given block itself
    // Only network core block is allowed to manipulate this variable
    var connected: Boolean

    // How much energy given block consumes from network core
    // maybe i should consider adding tiers of cores each with different bufferSize?
    // nah, no sense
    // big networks = big pain
    val energyRequired: Int

    // Meant to be used to diff facilities and cables
    val passthrough: Boolean
}