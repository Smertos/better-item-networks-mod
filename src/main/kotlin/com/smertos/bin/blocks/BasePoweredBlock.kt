@file:Suppress("LeakingThis")

package com.smertos.bin.blocks

import com.smertos.bin.BetterItemNetworksMain.debug
import com.smertos.bin.BetterItemNetworksMain.proxy
import com.smertos.bin.BetterItemNetworksMain.tabBIN
import com.smertos.bin.tileentity.BasePoweredTileEntity
import net.minecraft.block.Block
import net.minecraft.block.ITileEntityProvider
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.BlockRenderLayer
import net.minecraft.util.EnumBlockRenderType
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.math.BlockPos
import net.minecraft.util.text.TextComponentString
import net.minecraft.world.World

open class BasePoweredBlock(blockName: String, energyBSize: Int) : Block(Material.ROCK), ITileEntityProvider {

    val bName = "block.$blockName"

    init {
        setRegistryName(bName)
        unlocalizedName = bName

        if(debug || blockName != "base") {
            setCreativeTab(tabBIN)
        }
    }

    val energyBufferSize: Int = energyBSize


    fun registerItemModel(itemBlock: Item) = proxy?.registerItemRenderer(itemBlock, 0, bName)

    override fun getBlockLayer(): BlockRenderLayer = BlockRenderLayer.SOLID

    override fun createNewTileEntity(world: World?, meta: Int): TileEntity = BasePoweredTileEntity(energyBufferSize)

    override fun onBlockActivated(world: World?, pos: BlockPos?, state: IBlockState?, player: EntityPlayer?, hand: EnumHand?, heldItem: ItemStack?, side: EnumFacing?, hitX: Float, hitY: Float, hitZ: Float): Boolean {
        if(world != null && !world.isRemote) {
            val te = world.getTileEntity(pos) as BasePoweredTileEntity?
            if(te != null)
                player?.addChatComponentMessage(TextComponentString("${te.ec.energyStored}/${te.ec.maxEnergyStored} RF"))
        }

        return true
    }
}