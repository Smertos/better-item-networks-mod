package com.smertos.bin.blocks

import com.smertos.bin.network.INetworkable
import com.smertos.bin.tileentity.BasePoweredTileEntity
import net.minecraft.util.IStringSerializable
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

const val ID = "network.core"
const val bufferSize = 640_000

class NetworkCoreBlock : BasePoweredBlock(ID, bufferSize), INetworkable {

    companion object {
        enum class HasPower(i: Boolean): IStringSerializable {
            FALSE(false),
            TRUE(true);

            override fun getName(): String {
                return if (this.ordinal == 1) "true" else "false"
            }

            override fun toString(): String {
                return getName()
            }
        }
    }

    override var connected: Boolean = false
    override val energyRequired: Int = 4
    override val passthrough: Boolean = false

    @SideOnly(Side.CLIENT)
    fun isCorePowered(world: World, pos: BlockPos): Boolean {
        if(world.isRemote) {
            val te = world.getTileEntity(pos) as BasePoweredTileEntity
            return te.ec.energyStored > 0
        }

        return false
    }

    //val hasPowerType: PropertyEnum<HasPower> = PropertyEnum.create("hasPower", java.lang.Enum.valueOf(HasPower.va, java))


    /*override fun getActualState(state: IBlockState?, worldIn: IBlockAccess?, pos: BlockPos?): IBlockState {

        return state.withProperty(state, worldIn, pos)
    }*/

}