package com.smertos.bin.tileentity

import cofh.api.energy.EnergyStorage
import cofh.api.energy.IEnergyReceiver
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.EnumFacing

class BasePoweredTileEntity(energyBufferSize: Int = 64000): TileEntity(), IEnergyReceiver {

    val ec = EnergyStorage(energyBufferSize, Int.MAX_VALUE - 1)

    fun getEnergyStorage() = ec

    override fun canConnectEnergy(from: EnumFacing?): Boolean = true

    override fun getEnergyStored(from: EnumFacing?): Int = ec.energyStored

    override fun getMaxEnergyStored(from: EnumFacing?): Int = ec.maxEnergyStored

    override fun receiveEnergy(from: EnumFacing?, maxReceive: Int, simulate: Boolean): Int
            = ec.receiveEnergy(maxReceive, simulate)

    override fun getUpdateTag(): NBTTagCompound {
        val tag = NBTTagCompound()
        writeToNBT(tag)
        return tag
    }

    override fun handleUpdateTag(tag: NBTTagCompound) {
        readFromNBT(tag)
    }

    override fun writeToNBT(tag: NBTTagCompound): NBTTagCompound {
        super.writeToNBT(tag)

        tag.setInteger("energyStored", ec.energyStored)
        tag.setInteger("maxEnergyStored", ec.maxEnergyStored)

        return tag
    }

    override fun readFromNBT(compound: NBTTagCompound) {
        super.readFromNBT(compound)

        ec.energyStored = compound.getInteger("energyStored")
        ec.setCapacity(compound.getInteger("maxEnergyStored")) //TODO: Start making Network Cores
    }
}